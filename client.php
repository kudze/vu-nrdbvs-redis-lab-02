<?php

require __DIR__ . '/vendor/autoload.php';

use Kudze\NrbdvsMongo\Commands\CommandManager;
use Kudze\NrbdvsMongo\Commands\RegisterUserCommand;
use Kudze\NrbdvsMongo\Commands\LoginUserCommand;
use Kudze\NrbdvsMongo\Commands\LogoutUserCommand;
use Kudze\NrbdvsMongo\Commands\ExitCommand;
use Kudze\NrbdvsMongo\Commands\ListAllUsersCommand;
use Kudze\NrbdvsMongo\Commands\CreateCompanyCommand;
use Kudze\NrbdvsMongo\Commands\ListAllCompaniesCommand;
use Kudze\NrbdvsMongo\Commands\ListAllBillsCommand;
use Kudze\NrbdvsMongo\Commands\ListMyBillsCommand;
use Kudze\NrbdvsMongo\Commands\CreateBillCommand;
use Kudze\NrbdvsMongo\Commands\ClearCommand;
use Kudze\NrbdvsMongo\Commands\HelpCommand;
use Kudze\NrbdvsMongo\Commands\RemoveCurrentUserCommand;
use Kudze\NrbdvsMongo\Commands\AggregateUsersBalancesCommand;
use Kudze\NrbdvsMongo\Commands\AggregateUsersBalancesCommand2;
use Kudze\NrbdvsMongo\Commands\SetUsersBalanceCommand;
use Kudze\NrbdvsMongo\Services\Logger;
use Kudze\NrbdvsMongo\Services\Inputter;
use Kudze\NrbdvsMongo\Exceptions\InvalidCommandException;

$container = new DI\Container();

//Lets register all commands we need.
$cmdManager = $container->get(CommandManager::class);
$cmdManager->registerCommand($container->get(HelpCommand::class));
$cmdManager->registerCommand($container->get(RegisterUserCommand::class));
$cmdManager->registerCommand($container->get(LoginUserCommand::class));
$cmdManager->registerCommand($container->get(ListAllUsersCommand::class));
$cmdManager->registerCommand($container->get(ExitCommand::class));
$cmdManager->registerCommand($container->get(ClearCommand::class));
$cmdManager->registerCommand($container->get(LogoutUserCommand::class));
$cmdManager->registerCommand($container->get(CreateCompanyCommand::class));
$cmdManager->registerCommand($container->get(ListAllCompaniesCommand::class));
$cmdManager->registerCommand($container->get(CreateBillCommand::class));
$cmdManager->registerCommand($container->get(ListMyBillsCommand::class));
$cmdManager->registerCommand($container->get(ListAllBillsCommand::class));
$cmdManager->registerCommand($container->get(RemoveCurrentUserCommand::class));
$cmdManager->registerCommand($container->get(AggregateUsersBalancesCommand::class));
$cmdManager->registerCommand($container->get(AggregateUsersBalancesCommand2::class));
$cmdManager->registerCommand($container->get(SetUsersBalanceCommand::class));

$inputter = $container->get(Inputter::class);
$logger = $container->get(Logger::class);
$logger->println("Hello to interactive tax collector!");
$logger->println("Write help if you need any help with the tool.");
$logger->println("* Have fun! :)");
$logger->println("* Credits: Karolis Kraujelis");
$logger->println();

while(true) {
    $input = $inputter->askForInput();

    try {
        $cmdManager->parseInput($input);
    } catch(InvalidCommandException $e) {
        $logger->println($e->getCommand() . ' command doesnt exist!');
    }
}


