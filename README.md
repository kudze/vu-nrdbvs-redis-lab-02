# Small MongoDB ORM demo.

Two entities in separate collections defined. Also, there's an embedded entity Bill.

## Requirements

* PHP 8
* MongoDB
* Composer

## Running

1. Clone the repository.
2. Run `composer install` to install needed dependencies.
3. Run `php client.php` and have fun!


## Mongo

By default, it will attempt to connect to mongo instance running on localhost with default mongodb port with no user specified.
It will use `mongo_demo` database. If you want to change any of this refer to `MongoConnector` service.