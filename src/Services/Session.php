<?php

namespace Kudze\NrbdvsMongo\Services;

use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;

class Session
{
    private ?User $user = null;
    private Repository $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function setLoggedInUser(?User $user)
    {
        $this->user = $user;
    }

    public function getLoggedInUser(): ?User
    {
        return $this->user;
    }

    public function isLoggedInToAnyUser(): bool
    {
        return $this->getLoggedInUser() !== null;
    }

    public function refreshUser(): ?User
    {
        if (!$this->isLoggedInToAnyUser())
            return null;

        $this->user = $this->repository->find(User::class, $this->user->getId());
        if ($this->user === null)
            $this->repository->getLogger()->println("After Session::refreshUser, it was determined that currently logged in user, no longer exists!");

        return $this->getLoggedInUser();
    }
}