<?php

namespace Kudze\NrbdvsMongo\Services;

use Kudze\NrbdvsMongo\Exceptions\ValidationException;
use Kudze\NrbdvsMongo\Models\Company;

class Inputter
{
    private Session $session;
    private Logger $logger;
    private TablePrinter $tablePrinter;

    public function __construct(Session $session, Logger $logger, TablePrinter $tablePrinter)
    {
        $this->logger = $logger;
        $this->session = $session;
        $this->tablePrinter = $tablePrinter;
    }


    public function askForInput(): string
    {
        return readline($this->getPrompt());
    }

    public function askForInputWithPrompt(string $prompt): string
    {
        $this->logger->println();
        $this->logger->println($prompt);
        return $this->askForInput();
    }

    protected function getPrompt()
    {
        if (!$this->session->isLoggedInToAnyUser())
            return '*: ';

        return $this->session->getLoggedInUser()->getEmail() . ": ";
    }

    /**
     * @param Logger $logger
     * @param callable $validator validator(string $value) (on error should throw ValidationException).
     */
    public function askForValidatedInput(callable $validator, string $prompt = "Enter value:")
    {
        while (true) {
            $this->logger->println();
            $this->logger->println($prompt);
            $value = $this->askForInput();

            try {
                $validator($value);
            } catch (ValidationException $exception) {
                $this->logger->println($exception->getValidationMessage());
                continue;
            }

            return $value;
        }
    }

    public function askForEmail(string $failMsg = "Email address isn't valid!"): string
    {
        return $this->askForValidatedInput(
            function (string $value) use ($failMsg) {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL))
                    throw new ValidationException($failMsg);
            },
            'Enter email address:'
        );
    }

    public function askForEmailIn(array $emails, string $failInMsg = "Given email isn't in given options!", string $failFormatMsg = "Email address isn't valid!"): string
    {
        return $this->askForValidatedInput(
            function (string $value) use ($emails, $failInMsg, $failFormatMsg) {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL))
                    throw new ValidationException($failFormatMsg);

                if(!in_array($value, $emails))
                    throw new ValidationException($failInMsg);
            },
            'Enter email address:'
        );
    }

    public function askForAnyTextInLength(int $minLen, int $maxLen, string $prompt = "Enter value:") : string
    {
        return $this->askForValidatedInput(
            function (string $value) use ($minLen, $maxLen) {
                $value_len = strlen($value);

                if ($value_len < $minLen)
                    throw new ValidationException("Entered input is too short (Min characters needed: $minLen)!");

                if ($value_len > $maxLen)
                    throw new ValidationException("Entered input is too long (Max characters needed: $maxLen)!");
            },
            $prompt
        );
    }

    public function askForCompanyID(array $companies): string
    {
        $companyIds = [];
        /** @var Company $company */
        foreach ($companies as $company)
            $companyIds[] = $company->getId();

        $this->logger->println("List of all companies:");
        $this->tablePrinter->printCompanies($companies);

        return $this->askForValidatedInput(
            function (string $value) use ($companyIds) {
                if (!in_array($value, $companyIds))
                    throw new ValidationException("Company ID is not valid!");
            },
            "Enter company Id:"
        );
    }

    public function askForUserEmail(array $users): string
    {
        $emails = [];
        foreach ($users as $user)
            $emails[] = $user->getEmail();

        $this->logger->println("List of all users:");
        $this->tablePrinter->printUsers($users);

        return $this->askForValidatedInput(
            function (string $value) use ($emails) {
                if (!in_array($value, $emails))
                    throw new ValidationException("Company ID is not valid!");
            },
            "Enter user's email:"
        );
    }

    public function askForPositiveFloat(string $prompt = "Enter positive float:"): float
    {
        $res = $this->askForValidatedInput(
            function (string $value) {
                if(!is_numeric($value))
                    throw new ValidationException('Entered value must be numeric!');

                $value = (float)$value;

                if ($value < 0)
                    throw new ValidationException('Entered value must be positive!');
            },
            $prompt
        );

        return (float)$res;
    }
}