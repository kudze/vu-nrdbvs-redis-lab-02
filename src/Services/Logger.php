<?php

namespace Kudze\NrbdvsMongo\Services;

class Logger
{
    public function println(string $message = '')
    {
        echo $message . "\n";
    }

    public function printdebugln(string $message = '')
    {
        echo "[DEBUG] " . $message . "\n";
    }
}