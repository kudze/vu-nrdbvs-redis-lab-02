<?php

namespace Kudze\NrbdvsMongo\Services;

use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Database;

class MongoConnector
{
    private Client $mongo;

    public function __construct(string $ip = '127.0.0.1', int $port = 27017)
    {
        $this->mongo = new Client(
            'mongodb://' . $ip . ':' . $port . '/'
        );
    }

    /**
     * @return Client
     */
    public function getMongo(): Client
    {
        return $this->mongo;
    }

    public function getDatabase(string $databaseName = "mongo_demo", array $options = []): Database
    {
        return $this->getMongo()->selectDatabase($databaseName, $options);
    }

    public function getCollection(Database $database, string $collectionName): Collection
    {
        return $database->selectCollection($collectionName);
    }
}