<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;

class HelpCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'help',
            'Lists all available commands',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $manager = $this->getContainer()->get(CommandManager::class);
        $commands = $manager->getRegisteredCommands();
        $commandCount = count($commands);

        $logger->println("There are $commandCount commands in total!");
        foreach($commands as $command) {
            $logger->println($command->getKeyLower() . ' - ' . $command->getDescription());
        }
        $logger->println();
    }
}