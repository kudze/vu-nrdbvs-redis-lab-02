<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class ListAllUsersCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'users',
            'Lists all users',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();

        $users = $repository->findAll(User::class);

        if (empty($users)) {
            $logger->println("No users registered yet!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $tablePrinter->printUsers($users);
    }
}