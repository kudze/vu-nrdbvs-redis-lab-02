<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Exceptions\ModelAlreadyExistsException;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Hasher;

class RegisterUserCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'register',
            'Registers an user',
            $container
        );
    }

    public function run(string $params)
    {
        $hasher = $this->getContainer()->get(Hasher::class);
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();
        $inputter = $this->getInputter();

        $email = $inputter->askForEmail();
        $password = $inputter->askForAnyTextInLength(3, 64, 'Enter password:');
        $firstName = $inputter->askForAnyTextInLength(3, 64, 'Enter first name:');
        $lastName = $inputter->askForAnyTextInLength(3, 64, 'Enter last name:');

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($hasher->hash($password));
        $user->setFirstName($firstName);
        $user->setLastName($lastName);

        try {
            $repository->insert($user);

            $logger->println("User was successfully registered, you may log in now!");
        } catch(ModelAlreadyExistsException) {
            $logger->println("This email address is already in use!");
        }
    }
}