<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;

class ExitCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'exit',
            'terminates the program',
            $container
        );
    }

    public function run(string $params)
    {
        exit(0);
    }
}