<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Company;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class ListAllCompaniesCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'companies',
            'Lists all companies',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();

        $companies = $repository->findAll(Company::class);

        if (empty($companies)) {
            $logger->println("No companies exist yet!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $tablePrinter->printCompanies($companies);
    }
}