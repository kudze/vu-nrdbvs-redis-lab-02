<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Bill;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Repositories\UserRepository;
use Kudze\NrbdvsMongo\Services\TablePrinter;
use LucidFrame\Console\ConsoleTable;

class AggregateUsersBalancesCommand2 extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'ag_ub2',
            'Aggregates all users balance',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(UserRepository::class);

        $balances = $repository->aggregateUsersBalancesWithMapReduce();

        $table = new ConsoleTable();
        $table
            ->addHeader('User email')
            ->addHeader('Unpaid bills balance')
            ->addHeader('Users balance')
            ->addHeader('Calculated users balance');
        foreach ($balances as $row) {
            $value = $row['value'];

            $table->addRow()
                ->addColumn($row['_id'])
                ->addColumn($value['bills_balance'])
                ->addColumn($value['user_balance'])
                ->addColumn($value['balance']);
        }

        $table->setPadding(2);
        $table->display();
    }
}