<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;

class ClearCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'clear',
            'Clears screen :DD I dont want to have command that clears database xd',
            $container
        );
    }

    public function run(string $params)
    {
        for($i = 0; $i < 20; $i++)
            $this->getLogger()->println("");
    }
}