<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Exceptions\ModelAlreadyExistsException;
use Kudze\NrbdvsMongo\Exceptions\ModelDoesntExistException;
use Kudze\NrbdvsMongo\Exceptions\ValidationException;
use Kudze\NrbdvsMongo\Models\Bill;
use Kudze\NrbdvsMongo\Models\Company;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Session;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class CreateBillCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'cbill',
            'Creates a bill',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();
        $inputter = $this->getInputter();

        $companies = $repository->findAll(Company::class);
        $companyID = $inputter->askForCompanyID($companies);

        $users = $repository->findAll(User::class);
        $userEmail = $inputter->askForUserEmail($users);

        $amount = $inputter->askForPositiveFloat("Enter ammount:");

        //In production we would want to wrap this in transaction or smth.
        /** @var User $user */
        $user = $repository->find(User::class, $userEmail);
        if ($user === null) {
            $logger->println("User with given email no longer exists!");
            return;
        }

        $bill = new Bill();
        $bill->setCompanyId($companyID);
        $bill->setAmount($amount);
        $user->addBill($bill);

        try {
            $repository->update($user);

            //If we add a bill to currently logged in user lets update data in session too.
            $session = $this->getContainer()->get(Session::class);
            if ($session->getLoggedInUser()?->getId() === $user->getId())
                $session->setLoggedInUser($user);

            $logger->println("Bill has been successfully created!");
        } catch(ModelDoesntExistException) {
            $logger->println("We couldn't not update given user!");
        }
    }
}