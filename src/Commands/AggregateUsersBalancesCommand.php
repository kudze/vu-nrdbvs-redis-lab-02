<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Bill;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Repositories\UserRepository;
use Kudze\NrbdvsMongo\Services\TablePrinter;
use LucidFrame\Console\ConsoleTable;

class AggregateUsersBalancesCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'ag_ub',
            'Aggregates all users balance',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(UserRepository::class);

        $balances = $repository->aggregateUsersBalances();

        $table = new ConsoleTable();
        $table
            ->addHeader('User email')
            ->addHeader('Unpaid bills balance')
            ->addHeader('Users balance')
            ->addHeader('Calculated users balance');
        foreach ($balances as $row) {
            $table->addRow()
                ->addColumn($row['_id'])
                ->addColumn($row['bills_balance'])
                ->addColumn($row['user_balance'])
                ->addColumn($row['balance']);
        }

        $table->setPadding(2);
        $table->display();
    }
}