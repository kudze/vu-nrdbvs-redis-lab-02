<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Exceptions\LoginCredentialsInvalidException;
use Kudze\NrbdvsMongo\Exceptions\ModelDoesntExistException;
use Kudze\NrbdvsMongo\Exceptions\ModelNotFoundException;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Hasher;
use Kudze\NrbdvsMongo\Services\Session;

class RemoveCurrentUserCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'removeme',
            'Removes an current user',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class); //<-- More like fake session.

        if(!$session->isLoggedInToAnyUser())
        {
            $logger->println("You aren't logged in!");
            return;
        }

        try {
            $repository = $this->getContainer()->get(Repository::class);
            $repository->delete($session->getLoggedInUser());
            $session->setLoggedInUser(null);

            $logger->println("Current user has been removed!");
        } catch(ModelDoesntExistException) {
            $logger->println("Failed to remove current user!");
        }
    }
}