<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Bill;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Repositories\UserRepository;
use Kudze\NrbdvsMongo\Services\Session;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class ListMyBillsCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'mybills',
            'Lists your bills',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class);

        if(!$session->isLoggedInToAnyUser())
        {
            $logger->println("You arent logged in to any user!");
            return;
        }

        $user = $session->getLoggedInUser();
        $bills = $user->getBills();

        if (empty($bills)) {
            $logger->println("No bills for current user created yet!");
            return;
        }

        $logger->println("Current user's bills:");
        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $tablePrinter->printBills($bills);
    }
}