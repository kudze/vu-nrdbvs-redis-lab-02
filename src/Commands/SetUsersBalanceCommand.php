<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Exceptions\LoginCredentialsInvalidException;
use Kudze\NrbdvsMongo\Exceptions\ModelDoesntExistException;
use Kudze\NrbdvsMongo\Exceptions\ModelNotFoundException;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Hasher;
use Kudze\NrbdvsMongo\Services\Session;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class SetUsersBalanceCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'update_user_balance',
            'Sets users balance',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $inputter = $this->getInputter();
        $repository = $this->getContainer()->get(Repository::class);
        $tablePrinter = $this->getContainer()->get(TablePrinter::class);

        $users = $repository->findAll(User::class);
        $logger->println("All users:");
        $tablePrinter->printUsers($users);

        $usersIds = [];
        /** @var User $user */
        foreach($users as $user)
            $usersIds[] = $user->getEmail();

        $email = $inputter->askForEmailIn($usersIds, "Given email isn't linked to any user!");
        $balance = $inputter->askForPositiveFloat('Enter new balance:');

        try {
            $user = $repository->find(User::class, $email);
            if($user === null)
                throw new ModelDoesntExistException(null);

            $user->setBalance($balance);
            $repository->update($user);

            $logger->println("Model has been updated!");
        } catch(ModelDoesntExistException) {
            $logger->println("Invalid user email given!");
        }
    }
}