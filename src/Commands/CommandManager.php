<?php

namespace Kudze\NrbdvsMongo\Commands;

use InvalidArgumentException;
use Kudze\NrbdvsMongo\Exceptions\InvalidCommandException;

class CommandManager
{
    private array $commands = [];

    public function registerCommand(AbstractCommand $command)
    {
        if ($this->isCommandRegistered($command->getKeyLower()))
            throw new InvalidArgumentException(
                'Command with key ' . $command->getKeyLower() . ' is already registered in command manager!',
                500
            );

        $this->commands[$command->getKeyLower()] = $command;
    }

    /**
     * Parses command input.
     *
     * @param string $message
     * @throws InvalidCommandException
     */
    public function parseInput(string $message)
    {
        [
            'cmd' => $command,
            'args' => $args
        ] = $this->parseInputIntoArgs($message);

        $cmd = $this->getRegisteredCommand($command);
        $cmd->run($args);
    }

    /**
     * Returns wherether command is registered.
     *
     * @param string $key
     * @return bool
     */
    public function isCommandRegistered(string $key) : bool
    {
        return array_key_exists($key, $this->commands);
    }

    /**
     * Returns registered command, if no command then throws exception.
     *
     * @param string $key
     * @return AbstractCommand
     * @throws InvalidCommandException
     */
    public function getRegisteredCommand(string $key) : AbstractCommand
    {
        if(!$this->isCommandRegistered($key))
            throw new InvalidCommandException($key);

        return $this->commands[$key];
    }

    public function getRegisteredCommands(): array
    {
        return $this->commands;
    }

    protected function parseInputIntoArgs(string $message): array
    {
        if (!str_contains($message, ' '))
            return [
                'cmd' => strtolower($message),
                'args' => ''
            ];

        $data = explode(' ', $message, 2);

        return [
            'cmd' => strtolower($data[0]),
            'args' => $data[1]
        ];
    }

}