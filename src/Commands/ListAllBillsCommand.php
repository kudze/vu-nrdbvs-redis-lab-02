<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Bill;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Repositories\UserRepository;
use Kudze\NrbdvsMongo\Services\TablePrinter;

class ListAllBillsCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'bills',
            'Lists all bills',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(UserRepository::class);
        $logger = $this->getLogger();

        $bills = $repository->getAllBills();
        if (empty($bills)) {
            $logger->println("No bills created yet!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $logger->println("All bills:");
        $tablePrinter->printBills($bills);
    }
}