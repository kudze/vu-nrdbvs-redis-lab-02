<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Models\Company;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Hasher;
use Kudze\NrbdvsMongo\Services\Inputter;
use Kudze\NrbdvsMongo\Services\Logger;

class CreateCompanyCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'ccompany',
            'Creates a company',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();
        $inputter = $this->getInputter();

        $title = $inputter->askForAnyTextInLength(3, 256, "Enter company title:");

        $company = new Company();
        $company->setTitle($title);
        $repository->insert($company);

        $logger->println("Company has been successfully created!");
    }
}