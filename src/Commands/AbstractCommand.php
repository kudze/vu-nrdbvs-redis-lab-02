<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Services\Inputter;
use Kudze\NrbdvsMongo\Services\Logger;

abstract class AbstractCommand
{
    private string $key;
    private string $description;
    private Container $container;

    public function __construct(
        string $key,
        string $description,
        Container $container
    ) {
        $this->key = $key;
        $this->description = $description;
        $this->container = $container;
    }

    public abstract function run(string $params);

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * Returns key in lower case.
     *
     * @return string
     */
    public function getKeyLower(): string
    {
        return strtolower($this->key);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    public function getLogger(): Logger
    {
        return $this->container->get(Logger::class);
    }

    public function getInputter(): Inputter
    {
        return $this->container->get(Inputter::class);
    }
}