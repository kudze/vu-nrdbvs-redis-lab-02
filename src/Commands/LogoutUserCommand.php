<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Services\Session;

class LogoutUserCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'logout',
            'Logs out',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class); //<-- More like fake session.

        if (!$session->isLoggedInToAnyUser()) {
            $logger->println("You aren't logged in!");
            return;
        }

        $session->setLoggedInUser(null);
        $logger->println("Bye bye!");
    }
}