<?php

namespace Kudze\NrbdvsMongo\Commands;

use DI\Container;
use Kudze\NrbdvsMongo\Exceptions\LoginCredentialsInvalidException;
use Kudze\NrbdvsMongo\Exceptions\ModelNotFoundException;
use Kudze\NrbdvsMongo\Models\User;
use Kudze\NrbdvsMongo\Repositories\Repository;
use Kudze\NrbdvsMongo\Services\Hasher;
use Kudze\NrbdvsMongo\Services\Session;

class LoginUserCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'login',
            'Log in an user',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class); //<-- More like fake session.

        if($session->isLoggedInToAnyUser())
        {
            $logger->println("You are already logged in!");
            return;
        }

        $inputter = $this->getInputter();
        $repository = $this->getContainer()->get(Repository::class);
        $hasher = $this->getContainer()->get(Hasher::class);

        $email = $inputter->askForEmail();
        $password = $inputter->askForInputWithPrompt('Enter password:');

        try {
            $user = $repository->find(User::class, $email);
            if($user === null)
                throw new LoginCredentialsInvalidException();

            if(!$hasher->verify($user->getPassword(), $password))
                throw new LoginCredentialsInvalidException();

            $session->setLoggedInUser($user);
        } catch(LoginCredentialsInvalidException) {
            $logger->println("Invalid credentials");
            return;
        }

        $logger->println("Welcome, " . $user->getEmail());
    }
}