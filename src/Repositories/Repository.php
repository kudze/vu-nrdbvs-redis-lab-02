<?php

namespace Kudze\NrbdvsMongo\Repositories;

use InvalidArgumentException;
use Kudze\NrbdvsMongo\Exceptions\ModelAlreadyExistsException;
use Kudze\NrbdvsMongo\Exceptions\ModelDoesntExistException;
use Kudze\NrbdvsMongo\Models\AbstractModel;
use Kudze\NrbdvsMongo\Services\Logger;
use Kudze\NrbdvsMongo\Services\MongoConnector;
use MongoDB\Driver\Exception\BulkWriteException;

class Repository
{
    private MongoConnector $connector;
    private Logger $logger;

    public function __construct(MongoConnector $connector, Logger $logger)
    {
        $this->connector = $connector;
        $this->logger = $logger;
    }

    /**
     * @param string $model_class
     * @param mixed $key
     * @return AbstractModel|null
     */
    public function find(string $model_class, mixed $key): ?AbstractModel
    {
        if(!is_subclass_of($model_class, AbstractModel::class))
            throw new InvalidArgumentException("model_class should be a subclass of AbstractModel");

        /** @var AbstractModel $model_class */
        $collectionName = $model_class::getCollectionName();
        $database = $this->connector->getDatabase();
        $collection = $this->connector->getCollection($database, $collectionName);

        $result = $collection->findOne([$model_class::getIdFieldName() => $key]);
        if ($result === null)
            return null;

        /** @var AbstractModel $model */
        $model = new $model_class();
        $model->parseFromDocument($result);
        return $model;
    }

    public function findAll(string $model_class): array
    {
        if(!is_subclass_of($model_class, AbstractModel::class))
            throw new InvalidArgumentException("model_class should be a subclass of AbstractModel");

        /** @var AbstractModel $model_class */
        $collectionName = $model_class::getCollectionName();
        $database = $this->connector->getDatabase();
        $collection = $this->connector->getCollection($database, $collectionName);

        $data = $collection->find([]);
        $result = [];

        foreach ($data->toArray() as $dataEntry) {
            $entry = new $model_class;
            $entry->parseFromDocument($dataEntry);

            $result[] = $entry;
        }

        return $result;
    }

    /**
     * @throws ModelAlreadyExistsException
     */
    public function insert(AbstractModel $model): void
    {
        $collectionName = $model::getCollectionName();
        $database = $this->connector->getDatabase();
        $collection = $this->connector->getCollection($database, $collectionName);

        try {
            $result = $collection->insertOne($model->parseToDocument());
            $model->setId($result->getInsertedId());
        } catch (BulkWriteException $exception) {
            if ($exception->getCode() === 11000)
                throw new ModelAlreadyExistsException($model);

            throw $exception;
        }
    }

    /**
     * @throws ModelDoesntExistException
     */
    public function update(AbstractModel $model): void
    {
        $collectionName = $model::getCollectionName();
        $database = $this->connector->getDatabase();
        $collection = $this->connector->getCollection($database, $collectionName);

        //Here if a lot of usage would go through app a model could track edited fields since last update.
        //And only update those, but for demo update all data is fine I think.
        $result = $collection->updateOne(
            [$model::getIdFieldName() => $model->getId()],
            ['$set' => $model->parseToDocument(true)]
        );

        if($result->getMatchedCount() == 0)
            throw new ModelDoesntExistException($model);
    }

    public function delete(AbstractModel $model)
    {
        $collectionName = $model::getCollectionName();
        $database = $this->connector->getDatabase();
        $collection = $this->connector->getCollection($database, $collectionName);

        $result = $collection->deleteOne(
            [$model::getIdFieldName() => $model->getId()]
        );

        if($result->getDeletedCount() == 0)
            throw new ModelDoesntExistException($model);
    }

    public function getConnector(): MongoConnector
    {
        return $this->connector;
    }

    public function getLogger(): Logger
    {
        return $this->logger;
    }
}