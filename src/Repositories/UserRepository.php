<?php

namespace Kudze\NrbdvsMongo\Repositories;

use Kudze\NrbdvsMongo\Models\User;
use MongoDB\BSON\Javascript;

class UserRepository extends Repository
{

    public function getAllBills(): array
    {
        $connector = $this->getConnector();
        $collection = $connector->getCollection($connector->getDatabase(), User::getCollectionName());

        $data = $collection->find(
            [],
            [
                'projection' => ['bills' => true]
            ]
        )->toArray();

        $bills = [];
        foreach ($data as $userData) {
            $user = new User();
            $user->parseFromDocument($userData);

            foreach ($user->getBills() as $bill)
                $bills[] = $bill;
        }

        return $bills;
    }

    public function aggregateUsersBalances(): array
    {
        $connector = $this->getConnector();
        $collection = $connector->getCollection($connector->getDatabase(), User::getCollectionName());

        return $collection->aggregate(
            [
                [
                    '$unwind' => '$bills'
                ],
                [
                    '$group' => [
                        '_id' => '$_id',
                        'bills_balance' => [
                            '$sum' => '$bills.amount',
                        ],
                        'user_balance' => [
                            '$min' => '$balance'
                        ]
                    ],
                ],
                [
                    '$addFields' => [
                        'balance' => [
                            '$subtract' => ['$user_balance', '$bills_balance']
                        ]
                    ]
                ]
            ]
        )->toArray();
    }

    public function aggregateUsersBalancesWithMapReduce(): array
    {
        $connector = $this->getConnector();
        $collection = $connector->getCollection($connector->getDatabase(), User::getCollectionName());

        $jsMapFn = <<<'EOD'
            function() { 
                let billsLen = this.bills.length;
            
                for(let i = 0; i < billsLen; i++) {
                    emit(this._id, {
                        bills_amounts: this.bills[i].amount,
                        balance: this.balance
                    });
                }
            }
        EOD;

        $jsReduceFn = <<<'EOD'
            function(key, values) {
                let valuesLen = values.length;
                let userBalance = values[0].balance;
                let billsBalance = 0;
                
                for(let i = 0; i < valuesLen; i++) {
                    billsBalance += values[i].bills_amounts;
                }
                
                let balance = userBalance - billsBalance;
                return {
                    bills_balance: billsBalance,
                    user_balance: userBalance,
                    balance: balance
                };
            }
        EOD;

        $res = $collection->mapReduce(
            new Javascript($jsMapFn),
            new Javascript($jsReduceFn),
            "balance"
        );

        return $res->getIterator()->toArray();
    }

}