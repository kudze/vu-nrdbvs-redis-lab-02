<?php

namespace Kudze\NrbdvsMongo\Exceptions;

class ValidationException extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message, 422);
    }

    /**
     * @return string
     */
    public function getValidationMessage(): string
    {
        return $this->getMessage();
    }
}