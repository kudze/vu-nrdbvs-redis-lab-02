<?php

namespace Kudze\NrbdvsMongo\Exceptions;

use Exception;

class LoginCredentialsInvalidException extends Exception
{
    public function __construct()
    {
        parent::__construct('Invalid credentials.');
    }
}