<?php

namespace Kudze\NrbdvsMongo\Exceptions;

use Exception;

class InvalidCommandException extends Exception
{
    private string $command;

    public function __construct(string $command)
    {
        parent::__construct("Command with name $command doesnt exist!");

        $this->command = $command;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }
}