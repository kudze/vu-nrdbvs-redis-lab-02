<?php

namespace Kudze\NrbdvsMongo\Exceptions;

use Exception;
use Kudze\NrbdvsMongo\Models\AbstractModel;

class ModelAlreadyExistsException extends Exception
{
    private AbstractModel $model;

    public function __construct(AbstractModel $model)
    {
        parent::__construct('Model in collection "' . $model::getCollectionName() . '" already exists with id of: "' . $model->getId() . '".');

        $this->model = $model;
    }

    /**
     * @return AbstractModel
     */
    public function getModel(): AbstractModel
    {
        return $this->model;
    }
}