<?php

namespace Kudze\NrbdvsMongo\Models;

use MongoDB\Model\BSONDocument;
use InvalidArgumentException;

/**
 * Is an embedded entity inside users.
 */
class Bill implements AbstractModelInterface
{
    protected ?string $companyId = null;
    protected float $amount = 0;

    //Not stored in db, put in on every fetch operation.
    protected ?string $userEmail = null;

    public function parseToDocument(bool $omitID = false): array
    {
        if($this->getCompanyId() === null)
            throw new InvalidArgumentException("Bill on save doesn't have company ID defined!");

        return [
            'companyId' => $this->getCompanyId(),
            'amount' => $this->getAmount()
        ];
    }

    public function parseFromDocument(BSONDocument $document)
    {
        $this->setCompanyId($document->companyId);
        $this->setAmount($document->amount);
    }

    public function getCompanyId(): ?string
    {
        return $this->companyId;
    }

    public function setCompanyId(string $company_id): void
    {
        $this->companyId = $company_id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(?string $userEmail): void
    {
        $this->userEmail = $userEmail;
    }
}