<?php

namespace Kudze\NrbdvsMongo\Models;

abstract class AbstractModel implements AbstractModelInterface
{
    protected static string $collection = "";
    protected static string $idField = "_id";

    public static function getCollectionName() : string {
        return static::$collection;
    }

    public static function getIdFieldName() : string {
        return static::$idField;
    }

    protected mixed $id = null;

    public function __construct(mixed $id = null)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId(): mixed
    {
        return $this->id;
    }

    public function isIdDefined(): bool
    {
        return $this->getId() !== null;
    }

    /**
     * @param mixed $id
     */
    public function setId(mixed $id): void
    {
        $this->id = $id;
    }
}