<?php

namespace Kudze\NrbdvsMongo\Models;

use MongoDB\Model\BSONDocument;

interface AbstractModelInterface
{
    public function parseFromDocument(BSONDocument $document);
    public function parseToDocument(bool $omitID = false): array;
}