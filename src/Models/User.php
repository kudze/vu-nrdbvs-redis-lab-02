<?php

namespace Kudze\NrbdvsMongo\Models;

use InvalidArgumentException;
use MongoDB\Model\BSONDocument;

class User extends AbstractModel
{
    protected static string $collection = "users";

    protected string $firstName = "";
    protected string $lastName = "";
    protected string $password = "";
    protected float $balance = 0;
    protected array $bills = [];

    public function parseFromDocument(BSONDocument $document)
    {
        $this->setId($document->{self::getIdFieldName()});

        if ($document->offsetExists('firstName'))
            $this->setFirstName($document->firstName);

        if ($document->offsetExists('lastName'))
            $this->setLastName($document->lastName);

        if ($document->offsetExists('password'))
            $this->setPassword($document->password);

        if ($document->offsetExists('balance'))
            $this->setBalance($document->balance);

        //Some users may have bills = null.
        if ($document->offsetExists('bills')) {
            foreach ($document->offsetGet('bills') as $billData) {
                $bill = new Bill();

                $bill->parseFromDocument($billData);
                $bill->setUserEmail($this->getId());

                $this->bills[] = $bill;
            }
        }
    }

    public function parseToDocument(bool $omitID = false): array
    {
        $bills = [];
        /** @var Bill $bill */
        foreach ($this->getBills() as $bill)
            $bills[] = $bill->parseToDocument();

        $result = [
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'password' => $this->getPassword(),
            'balance' => $this->getBalance(),
            'bills' => $bills
        ];

        if ($this->isIdDefined() && !$omitID)
            $result[self::getIdFieldName()] = $this->getId();

        return $result;
    }


    public function setEmail(string $email): void
    {
        $this->setId($email);
    }

    public function getEmail(): string
    {
        return $this->getId();
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setBalance(float $balance)
    {
        $this->balance = $balance;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function setBills(array $bills): void
    {
        $this->bills = $bills;
    }

    public function addBill(Bill $bill): void
    {
        if ($bill->getUserEmail() !== null)
            throw new InvalidArgumentException("One bill may only belong only to one user!");

        $this->bills[] = $bill;
        $bill->setUserEmail($this->getEmail());
    }

    public function getBills(): array
    {
        return $this->bills;
    }
}