<?php

namespace Kudze\NrbdvsMongo\Models;

use MongoDB\Model\BSONDocument;

class Company extends AbstractModel
{
    protected static string $collection = "companies";

    protected string $title = "";

    public function parseFromDocument(BSONDocument $document)
    {
        $this->setId($document->{self::getIdFieldName()});
        $this->setTitle($document->title);
    }

    public function parseToDocument(bool $omitID = false): array
    {
        $data = [
            'title' => $this->getTitle(),
        ];

        if($this->isIdDefined() && !$omitID)
            $data[self::getIdFieldName()] = $this->getId();

        return $data;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}